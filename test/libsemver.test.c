#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include "libsemver.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_from_string();
void test_to_string();
void test_to_int();
void test_compare();
void test_wildcards();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("libsemver", argc, argv);

  test("From String", test_from_string);
  test("To String", test_to_string);
  test("To Integer", test_to_int);
  test("Compare", test_compare);
  test("Wildcards", test_wildcards);

  return test_complete();
}

void test_from_string() {
  Semver *semver = semver_from_string("1.2.3");

  test_assert_int(semver->major, 1, "Should read the major part");
  test_assert_int(semver->minor, 2, "Should read the minor part");
  test_assert_int(semver->patch, 3, "Should read the patch part");
}

void test_to_string() {
  test_assert_string( semver_to_string(semver_new(0, 0, 0)), "0.0.0", "Should handle zeros" );
  test_assert_string( semver_to_string(semver_new(1, 2, 3)), "1.2.3", "Should serialise the version to a string" );
}

void test_to_int() {
  test_assert_int( semver_to_int(semver_new(0, 0, 0)), 0, "Should handle all zero version" );
  test_assert_int( semver_to_int(semver_new(1, 0, 1)), 100000001, "Should handle zero minor version" );
  test_assert_int( semver_to_int(semver_new(2, 12, 371)), 200120371, "Should handle large version numbers" );
}

void test_compare() {
  test_assert_int( semver_compare(semver_new(1, 2, 3), semver_new(1, 2, 3)), 0 , "Should return 0 when equal");
  test_assert_int( semver_compare(semver_new(0, 0, 6), semver_new(0, 0, 4)), 1 , "Should return 1 when patch version greater");
  test_assert_int( semver_compare(semver_new(0, 0, 3), semver_new(0, 0, 4)), -1 , "Should return -1 when patch version less");

  test_assert_int( semver_compare(semver_new(0, 19, 0), semver_new(0, 4, 0)), 1 , "Should return 1 when minor version greater");
  test_assert_int( semver_compare(semver_new(0, 12, 0), semver_new(0, 123, 0)), -1 , "Should return -1 when minor version less");

  test_assert_int( semver_compare(semver_new(11, 0, 0), semver_new(7, 0, 0)), 1 , "Should return 1 when major version greater");
  test_assert_int( semver_compare(semver_new(7, 0, 0), semver_new(11, 0, 0)), -1 , "Should return -1 when major version less");

  test_assert_int( semver_compare(semver_new(2, 0, 1), semver_new(1, 4, 72)), 1 , "Should return 1 when version greater");
  test_assert_int( semver_compare(semver_new(0, 1, 128), semver_new(0, 3, 0)), -1 , "Should return -1 when version less");
}

void test_wildcards() {
  test_assert_int(semver_from_string("1.2.*")->patch, -1, "Should replace wildcard with -1");

  test_assert_int( semver_compare(semver_from_string("1.2.*"), semver_new(1, 2, 9)), 0, "Should return equal when patch wildcard matches");
  test_assert_int( semver_compare(semver_from_string("1.*.3"), semver_new(1, 9, 3)), 0, "Should return equal when minor wildcard matches");
  test_assert_int( semver_compare(semver_from_string("*.2.3"), semver_new(9, 2, 3)), 0, "Should return equal when major wildcard matches");
}
