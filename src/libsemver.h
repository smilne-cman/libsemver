#ifndef __libsemver__
#define __libsemver__

/* Includes *******************************************************************/

/* Types **********************************************************************/
typedef struct Semver {
  int major;
  int minor;
  int patch;
} Semver;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
Semver *semver_new(int major, int minor, int patch);
Semver *semver_from_string(const char *value);
int semver_compare(void *source, void *target);
char *semver_to_string(Semver *semver);
int semver_to_int(Semver *semver);

#endif
