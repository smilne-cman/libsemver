#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include "libsemver.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static int decimal_places(int num, int start);
static int shift(int num, int by);
static int power(int base, int exp);
static int get_value(char *value);
static int contains_wildcard(Semver *version);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Semver *semver_new(int major, int minor, int patch) {
  Semver *semver = (Semver *)malloc(sizeof(Semver));

  semver->major = major;
  semver->minor = minor;
  semver->patch = patch;

  return semver;
}

Semver *semver_from_string(const char *value) {
  char *copy = (char *)malloc(sizeof(char) * strlen(value));
  strcpy(copy, value);
  Semver *semver = semver_new(0, 0, 0);

  semver->major = get_value(strtok(copy, "."));
  semver->minor = get_value(strtok(NULL, "."));
  semver->patch = get_value(strtok(NULL, "."));

  free(copy);
  return semver;
}

int semver_compare(void *source, void *target) {
  if(!contains_wildcard(source) && !contains_wildcard(target)) {
    int value1 = semver_to_int((Semver *)source);
    int value2 = semver_to_int((Semver *)target);

    if(value1 > value2) return 1;
    if(value1 < value2) return -1;

    return 0;
  }

  Semver *sourceVersion = (Semver *)source;
  Semver *targetVersion = (Semver *)target;

  int majorDiff = sourceVersion->major - targetVersion->major;
  if(majorDiff != 0 && sourceVersion->major != -1 && targetVersion->major != -1) {
    return majorDiff;
  }

  int minorDiff = sourceVersion->minor - targetVersion->minor;
  if(minorDiff != 0 && sourceVersion->minor != -1 && targetVersion->minor != -1) {
    return minorDiff;
  }

  int patchDiff = sourceVersion->patch - targetVersion->patch;
  if(patchDiff != 0 && sourceVersion->patch != -1 && targetVersion->patch != -1) {
    return patchDiff;
  }

  return 0;
}

char *semver_to_string(Semver *semver) {
  char *result = (char *)malloc(sizeof(char) * 15);

  sprintf(result, "%i.%i.%i", semver->major, semver->minor, semver->patch);

  return result;
}

int semver_to_int(Semver *semver) {
  int patch = semver->patch;
  int minor = shift(semver->minor, 4);
  int major = shift(semver->major, 8);

  return major + minor + patch;
}

static int shift(int num, int by) {
  return num * power(10, by);
}

static int decimal_places(int num, int start) {
  float thing = (float)num / power(10, start);

  if(thing >= 1) return decimal_places(num, start + 1);

  return start;
}

static int power(int base, int exp) {
  if(exp == 0) return 1;
  if(exp % 2) return base * power(base, exp - 1);

  int temp = power(base, exp / 2);
  return temp * temp;
}

static int get_value(char *value) {
  if(!strcmp(value, "*")) {
    return -1;
  }

  return strtoimax(value, NULL, 10);
}

static int contains_wildcard(Semver *version) {
  return version->major == -1 ||
    version->minor == -1 ||
    version->patch == -1;
}
